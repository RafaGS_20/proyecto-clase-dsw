<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/IEntity.php"; // Interfaz que nos fuerza a usar un determinado método. 

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Esta clase es la que define las categorías de nuestras imágenes. 
class Categoria implements IEntity
{
    
    private $id; 

    private $nombre; 

    private $numImagenes;
    
    public function __construct( string $nombre = "", $id = 0, $numImagenes = 0)
    {

        $this->nombre = $nombre;

        $this->id = $id;

        $this->numImagenes = $numImagenes;

    }

    public function toArray(): array
    {
        return [

            "nombre"=>$this->getNombre(),

            "id"=>$this->getId(),

            "numImagenes"=>$this->getNumImagenes()
            
        ];
    }


/* -----------------------------------------------------------   Getter and setter..   -----------------------------------------------------------*/
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of numImagenes
     */ 
    public function getNumImagenes()
    {
        return $this->numImagenes;
    }

    /**
     * Set the value of numImagenes
     *
     * @return  self
     */ 
    public function setNumImagenes($numImagenes)
    {
        $this->numImagenes = $numImagenes;

        return $this;
    }
}

?>