<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/IEntity.php"; // Hará falta esta interfaz para ciertos procesos.

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Clase que define todas las imágenes de nuestra galería. 
class ImagenGaleria implements IEntity
{
    private $id;
    
    private $nombre;

    private $descripcion;

    private $categoria;

    private $numVisualizaciones;

    private $numLikes;

    private $numDownloads;

    const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";
    const RUTA_IMAGENES_GALLERY = "images/index/gallery/";

    public function __construct($id = 0, $nombre = "", $descripcion = "", $categoria = 0, $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0)
    {
        $this->id = $id;

        $this->nombre = $nombre;

        $this->descripcion = $descripcion;

        $this->categoria = $categoria;

        $this->numVisualizaciones = $numVisualizaciones;

        $this->numLikes = $numLikes;

        $this->numDownloads = $numDownloads;
    }

    public function toArray(): array
    {
        return [

            "id"=>$this->getId(), 

            "nombre"=>$this->getNombre(),

            "descripcion"=>$this->getDescripcion(),

            "categoria"=>$this->getCategoria(),

            "numVisualizaciones"=>$this->getNumVisualizaciones(),

            "numLikes"=>$this->getNumLikes(),

            "numDownloads"=>$this->getNumDownloads()
            
        ];
    }

/* -----------------------------------------------------------   Getter and Setter.   -----------------------------------------------------------*/


    public function __toString()
    {
        return $this->descripcion;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    public function getNumLikes()
    {
        return $this->numLikes;
    }

    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    public function getURLPortfolio(): string
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    public function getURLGallery(): string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */ 
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }
}
