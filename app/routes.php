<?php 

/* Este archivos es un simple array asociativo, para cada clave devuelve un controller que usar
 para acabar sacando una vista, es un enrutador casero. */
return [

 "Proyecto_DSW/home" => "app/controllers/index.php",

 "Proyecto_DSW" => "app/controllers/index.php",

 "Proyecto_DSW/about" => "app/controllers/about.php",

 "Proyecto_DSW/formulario" => "app/controllers/formulario.php",

 "Proyecto_DSW/blog" => "app/controllers/blog.php",

 "Proyecto_DSW/contact" => "app/controllers/contact.php",

 "Proyecto_DSW/galeria" => "app/controllers/galeria.php",

 "Proyecto_DSW/post" => "app/controllers/single_post.php"

];

?>