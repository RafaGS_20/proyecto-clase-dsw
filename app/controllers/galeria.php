<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// Utils: pequeñas inserciones de código con funcionalidades simples para la página. 
require_once __DIR__ . "/../../utils/File.php"; // Clase que gestiona los archivos. 

// Excepciones: usadas para recoger los diferentes errores en la app. 
require_once __DIR__ ."/../../exceptions/QueryException.php"; // Clase que maneja los errores producidos durante la interacción con la base de datos.
require_once __DIR__ . "/../../exceptions/FileException.php"; // Clase con los fallos de los archivos. 
require_once __DIR__ . "/../../exceptions/AppException.php"; // Clase que maneja los errores producidos por el contenedor de servicios. 
 
// Entity: son nuestras clases principales, contenidas en la página y en la base de datos. 
require_once __DIR__ ."/../../entity/ImagenGaleria.php"; // Clase con todos los archivos que tenemos.
require_once __DIR__ . "/../../entity/Categoria.php"; // Clase con las categorías. 

// Repository: gestionan la interacción entre nuestras clases y la base de datos. 
require_once __DIR__ . "/../../repository/CategoriaRepository.php"; // Clase que gestiona la interacción con la base de datos. 
require_once __DIR__ . "/../../repository/ImagenGaleriaRepository.php"; // Clase que gestiona la interacción con la base de datos. 

// Database: contiene datos y sentencias de interacción con la base de datos. 
require_once __DIR__ . "/../../database/Connection.php"; // Clase que gestiona la conexión con la base de datos. 
require_once __DIR__ . "/../../database/QueryBuilder.php"; // Clase que gestiona nuestras sentencias sql. 

// Core: Contenedor de servicio y precargas para el funcionamiento.  
require_once __DIR__ . "/../../core/App.php"; // Clase que gestiona el contenedor de servicio. 
require_once __DIR__ . "/../../core/bootstrap.php"; // Clase que gestiona la carga de la base de datos. 
require_once __DIR__ . "/../../core/helpers/FlashMessage.php"; // Clase para la persistencia de datos en sesiones. 

// Monolog: uso del monolog para la gestión de logs en nuestro servicio. 
 use Monolog\Logger; // Se usa para la creación y manejos de logs.
 use Monolog\Handler\StreamHandler; // Se usa para almacenar recursos en local.  

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Uso de Monolog en nuestra aplicación. 
$log = new Logger('galeria');
$log->pushHandler(new StreamHandler('logs/info.log', Logger::INFO));
// Variables para validación. 
$descripcion = FlashMessage::get("descripcion");
$mensaje = FlashMessage::get("mensaje");
/* $errores = array(); Variable inutilizada por actualización de métodos. */
$categoriaSeleccionada = FlashMessage::get("categoriaSeleccionada");


// Iniciamos un try en el que meteremos el funcionamiento de nuestra página: 
try {

    // Accedemos a nuestra clase creada para gestionar la base de datos y usamos el método de conexión. 
    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository();

    // En caso de que la página entre en un post: 
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Cogemos la variable de categoría por post. 
        $categoriaSeleccionada = trim(htmlspecialchars($_POST["categoria"]));
        FlashMessage::set("categoriaSeleccionada", $categoriaSeleccionada);
        // Cogemos la descripción de la página pero con seguridad.
        $descripcion = htmlspecialchars(trim(($_POST['descripcion']), " ")) ?? ''; 
        FlashMessage::set("descripcion", $descripcion);
        // Determinamos que tipo de archivos permitimos que se suban. 
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"]; 
        // Creamos el objeto imagen con el documento y el tipo de documento que es.
        $imagen = new File("imagen", $tiposAceptados); 
        // Lo guardamos dentro de la galería.
        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY); 
        // Realizamos una copia en el portfolio.
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);  
        // Crea un nuevo objeto imagen galería con el nombre y la descripción.
        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoriaSeleccionada);
        // Usamos nuestro query para guardar la imagen en la base de datos.
        $imagenGaleriaRepository->save($imagenGaleria);
        // Mensaje de salida si todo va bien, haciendo uso de sesiones.  
        FlashMessage::set("mensaje", "Se ha guardado la imagen en la BBDD.");
        // Se crea un registro en el archivo log.
        $log->info($mensaje);
        App::get("logger")->add($mensaje);
        // Reseteamos los parámetros en caso de realizarse la subida.  
        FlashMessage::unset("descripcion", $descripcion);
        FlashMessage::unset("descripcion", $descripcion);
        $descripcion = ""; 
        $categoriaSeleccionada = "";
    }

    // Ahora para cargar todas las imágenes realizamos una busquedad usando el método findAll de la clase. 
    $imagenes = $imagenGaleriaRepository->findAll();
    $categorias = $categoriaRepository->findAll();
    
}

/* -----------------------------------------------------------   Captura de errores.   -----------------------------------------------------------*/

catch (FileException $fileException) {

    /* Captura de errores dentro de un array 
    $errores[] = $fileException->getMessage(); */
    /* Captura de errores haciendo uso de las sesiones. 
    $_SESSION["errores"][] = $fileException->getMessage();*/
    // Captura de errores haciendo uso de Flash-message
    FlashMessage::set("errores", [$fileException->getMessage()]);
    
}
catch (QueryException $queryException) {

    /* Captura de errores dentro de un array 
    $errores[] = $queryException->getMessage(); */
    /* Captura de errores haciendo uso de las sesiones. 
    $_SESSION["errores"][] = $queryException->getMessage();*/
    // Captura de errores haciendo uso de Flash-message
    FlashMessage::set("errores", [$queryException->getMessage()]);
} 
catch (PDOException $pdoException) {

    /* Captura de errores dentro de un array 
    $errores[] = $pdoException->getMessage(); */
    /* Captura de errores haciendo uso de las sesiones.  
    $_SESSION["errores"][] = $pdoException->getMessage();*/
    // Captura de errores haciendo uso de Flash-message
    FlashMessage::set("errores", [$pdoException->getMessage()]);
}
catch (AppException $appException) {

    /* Captura de errores dentro de un array 
    $errores[] = $appException->getMessage(); */
    /* Captura de errores haciendo uso de las sesiones.
    $_SESSION["errores"][] = $appException->getMessage();*/
    // Captura de errores haciendo uso de Flash-message
    FlashMessage::set("errores", [$appException->getMessage()]);
}

// Con estas líneas evitaremos que un mismo mensaje se guarde más de una vez por sesión.
$errores = FlashMessage::get("errores");
unset($_SESSION["errores"]);
$mensaje = FlashMessage::get("mensaje");
unset($_SESSION["mensajes"]);
 
/* -----------------------------------------------------------   Llamada a la vista.   -----------------------------------------------------------*/
require __DIR__ . "/../views/galeria.view.php";
