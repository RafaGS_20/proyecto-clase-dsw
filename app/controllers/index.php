<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// Entity: son nuestras clases principales, contenidas en la página y en la base de datos. 
require_once "entity/ImagenGaleria.php"; // Clase con todos los archivos que tenemos.

// Repository: gestionan la interacción entre nuestras clases y la base de datos. 
require_once "repository/ImagenGaleriaRepository.php"; // Clase que gestiona la interacción con la base de datos. 

// Database: contiene datos y sentencias de interacción con la base de datos. 
require_once "database/Connection.php"; // Clase que gestiona la conexión con la base de datos. 
require_once "database/QueryBuilder.php"; // Clase que gestiona nuestras sentencias sql. 

// Utils: pequeñas inserciones de código con funcionalidades simples para la página. 
require_once "utils/File.php"; // Clase que gestiona los archivos.

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Array de imágenes a mostrar. 
$arrayImagenes = array();

// Creamos las variables pertinentes para atacar a la base de datos y buscar en ella. 
$imagenGaleriaRepository = new ImagenGaleriaRepository();
$imagenes = $imagenGaleriaRepository->findAll();

// Procedemos a la inserción desde la base de datos a la página. 
for ( $i = 0; $i <= count($imagenes)-1; $i++ ){
    $imagen_BBDD = new ImagenGaleria(
        $imagenes[$i]->getId(),
        $imagenes[$i]->getNombre(),
        $imagenes[$i]->getDescripcion(),
        $imagenes[$i]->getCategoria(),
        $imagenes[$i]->getNumVisualizaciones(),
        $imagenes[$i]->getNumLikes(),
        $imagenes[$i]->getNumDownloads() 
    );
    // Metemos todo en el array. 
    array_push( $arrayImagenes, $imagen_BBDD);
}

/* -----------------------------------------------------------   Llamada a la vista.   -----------------------------------------------------------*/
require __DIR__ . "/../views/index.view.php";

