<?php

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Variables de validación del formulario.  

$mensajeasunto = '';
$mensajeemail = '';
$mensajenombre = '';

$nombre = '';
$apellido = '';
$email = '';
$asunto = '';
$mensaje = '';

$nombreguardado='';
$apellidoguardado='';
$emailguardado='';
$asuntoguardado='';
$mensajeguardado='';

// Validación del formulario. 

if ($_SERVER['REQUEST_METHOD'] === 'POST' ) 
{
    $nombre = htmlspecialchars (trim ( ($_POST['nombre']) , " ")) ?? '';
    $apellido = htmlspecialchars (trim ( ($_POST['apellido']) , " ")) ?? '';
    $email = htmlspecialchars (trim ( ($_POST['email']) , " ")) ?? '';
    $asunto = htmlspecialchars (trim ( ($_POST['asunto']) , " ")) ?? '';
    $mensaje = htmlspecialchars (trim ( ($_POST['mensaje']) , " ")) ?? '';

    if (empty ($nombre)) {
        $emensajenombre = "El nombre es obligatorio.";
    }
    if (empty ($email)) {
        $mensajeemail = "El email es obligatorio.";
    } else if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $mensajeemail = "Debe introducir un email válido.";
    }
    if (empty ($asunto)) {
        $mensajeasunto = "El asunto es obligatorio.";
    }
    if ( empty ($nombre) === false && empty ($apellido) === false && empty ($email) === false && empty ($asunto) === false && empty ($mensaje) === false ) {
        $nombreguardado = "El nombre enviado es ".$nombre.".";
        $apellidoguardado = "El apellido enviado es ". $apellido.".";
        $emailguardado = "El email enviado es ".$email.".";
        $asuntoguardado = "El asunto enviado es ".$asunto.".";
        $mensajeguardado = "El mensaje guardado es ".$mensaje.".";
    }
}

/* -----------------------------------------------------------   Llamada a la vista.   -----------------------------------------------------------*/

require __DIR__ . "/../views/contact.view.php";
