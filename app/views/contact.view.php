<!-- Incluimos contenido por partials -->
<?php include __DIR__ . "/partials/inicio-doc.part.php"; ?>

<!-- Incluimos contenido por partials -->
<?php include __DIR__ . "/partials/nav.part.php"; ?>

<!-- Principal Content Start -->
   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>CONTACT US</h1>
       	   <hr>
       	   <p>Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
	       <form class="form-horizontal" method="POST">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Nombre</label>
	       	  		<input class="form-control" type="text" name="nombre" value="<?= $nombre ?>">
						 <p> <?= $mensajenombre ?></p>
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Apellido</label>
	       	  		<input class="form-control" type="text" name="apellido" value="<?= $apellido ?>">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Email</label>
	       	  		<input class="form-control" type="text" name="email" value="<?= $email ?>">
						 <p> <?= $mensajeemail ?></p>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Asunto</label>
	       	  		<input class="form-control" type="text" name="asunto" value="<?= $asunto ?>">
						 <p> <?= $mensajeasunto ?></p>
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Mensaje</label>
	       	  		<textarea class="form-control" name="mensaje" > <?= $mensaje ?> </textarea>
	       	  		<button class="pull-right btn btn-lg sr-button">Enviar</button>
	       	  	</div>
	       	  </div>
	       </form>
		   <div>
		   <label class="label-control"> <?= $nombreguardado ?> </label>
		   <label class="label-control"> <?= $apellidoguardado ?> </label>
		   <label class="label-control"> <?= $emailguardado ?> </label>
		   <label class="label-control"> <?= $asuntoguardado ?> </label>
		   <label class="label-control"> <?= $mensajeguardado ?> </label>
		   </div>
	       <hr class="divider">
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>

<!-- Principal Content Start -->

 <!-- Incluimos contenido por partials -->

 <?php include __DIR__ . "/partials/fin-doc.part.php"; ?>