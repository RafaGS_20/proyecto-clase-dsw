<?php

// Barajamos nuestro array de imágenes para cambiar el orden. 
shuffle($arrayImagenes);

?>


<div id="<?= $idCategoria ?>" class="tab-pane <?php if ( $idCategoria == "category1") echo "active" ?>">
  <div class="row popup-gallery">

    <!-- Este es el código que se repite para cada imagen -->
    <!-- Entramos en un bucle de tamaño del array para que meta el contenido -->
    <?php foreach ($arrayImagenes as $imagen) {
    ?>
      <!-- Cerramos php para trabajar el html y que durante todo el bucle use el mismo -->
      <div class="col-xs-12 col-sm-6 col-md-3">
        <div class="sol">
          <img class="img-responsive" src="<?= $imagen->getURLPortfolio() ?>" alt="<?= $imagen->getDescripcion() ?>">
          <div class="behind">
            <div class="head text-center">
              <ul class="list-inline">
                <li>
                  <a class="gallery" href="<?= $imagen->getURLGallery() ?>" data-toggle="tooltip" data-original-title="Quick View">
                    <i class="fa fa-eye"></i>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" data-original-title="Click if you like it">
                    <i class="fa fa-heart"></i>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" data-original-title="Download">
                    <i class="fa fa-download"></i>
                  </a>
                </li>
                <li>
                  <a href="#" data-toggle="tooltip" data-original-title="More information">
                    <i class="fa fa-info"></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="row box-content">
              <ul class="list-inline text-center">
                <li><i class="fa fa-eye"></i> <?= $imagen->getNumVisualizaciones() ?></li>
                <li><i class="fa fa-heart"></i><?= $imagen->getNumLikes() ?></li>
                <li><i class="fa fa-download"></i> <?= $imagen->getNumDownloads() ?></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Cerramos el foreach de arriba -->
    <?php
    }
    ?>
    <!-- Fin -->
  </div>


  <!-- Controles de paginación -->
  <nav class="text-center">
    <ul class="pagination">
      <li class="active"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#" aria-label="suivant">
          <span aria-hidden="true">&raquo;</span>
        </a></li>
    </ul>
  </nav>
</div>