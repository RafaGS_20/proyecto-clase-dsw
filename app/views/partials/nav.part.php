<?php include "/var/www/html/Proyecto_DSW/utils/utils.php"; ?>

<!-- Navigation Bar -->
<nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
         </div>
         <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
              <li class="<?php if ( esactiva("index") ) echo "active"; ?> lien"><a href="<?php if ( esactiva("index") ) echo "#"; else echo "home"; ?>"><i class="fa fa-home sr-icons"></i> Home</a></li>
              <li class="<?php if ( esactiva("about") ) echo "active"; ?> lien"><a href="<?php if ( esactiva("about") ) echo "#"; else echo "about"; ?>"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
              <li class="<?php if ( esactiva("blog") ) echo "active"; ?> lien"><a href="<?php if ( esactiva("blog") ) echo "#"; else echo "blog"; ?>"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
              <li class="<?php if ( esactiva("contact") ) echo "active"; ?> lien"><a href="<?php if ( esactiva("contact") ) echo "#"; else echo "contact"; ?>"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
              <li class="<?php if ( esactiva("formulario") ) echo "active"; ?> lien"><a href="<?php if ( esactiva("formulario") ) echo "#"; else echo "formulario"; ?>"><i class="fa fa-phone-square sr-icons"></i> Formulario</a></li>
              <li class="<?php if ( esactiva("galeria") ) echo "active"; ?> lien"><a href="<?php if ( esactiva("galeria") ) echo "#"; else echo "galeria"; ?>"><i class="fa fa-image"></i>Subir archivos</a></li>
            </ul>
         </div>
     </div>
   </nav>
<!-- End of Navigation Bar -->